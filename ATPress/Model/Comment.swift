//
//  Comment.swift
//  egemen.kz
//
//  Created by Toremurat on 28.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Comment {
    
    var id: Int
    var name: String
    var text: String
    var date: Int
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.text = json["text"].stringValue
        self.date = json["date"].intValue
    }
}

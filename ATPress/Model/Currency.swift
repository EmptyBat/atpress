//
//  Currency.swift
//  ernur.kz
//
//  Created by Toremurat on 02.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Currency {
    
    var title: String
    var pubDate: String
    var amount: String
    var desc: String
    
    init(json: JSON) {
        self.title = json["title"].stringValue
        self.pubDate = json["pubDate"].stringValue
        self.amount = json["description"].stringValue
        self.desc = json["label"].stringValue
    }
    
}

//
//  Images.swift
//  ATPress
//
//  Created by Almas Abdrasilov on 08.07.2018.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Images {
    
    var url: String
    
    
    init(json: JSON) {
        self.url = json["url"].stringValue
    }
    
}


//
//  Rubric.swift
//  infoBoom
//
//  Created by Toremurat on 03.01.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Rubric {
    
    var id: Int
    var name: String
    var hasLastArticles: Bool
    
    init?(json: JSON?) {
        guard let json = json,
        let id = json["id"].int,
        let name = json["name"].string,
        let hasLastArticles = json["hasLastArticles"].bool else {
            print("Rubric parsing error")
            return nil
        }
        
        self.id = id
        self.name = name
        self.hasLastArticles = hasLastArticles
    }
}

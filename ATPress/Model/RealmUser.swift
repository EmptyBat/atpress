//
//  RealmUser.swift
//  ernur.kz
//
//  Created by Toremurat on 10.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import RealmSwift

class RealmUser: Object{
    dynamic var id = 0
    dynamic var name = ""
    dynamic var text = ""
    dynamic var date = ""
    dynamic var image = ""
    dynamic var viewCount = 0
    dynamic var commentCount = 0
    dynamic var categoryName = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

//
//  Audio.swift
//  ernur.kz
//
//  Created by Toremurat on 02.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Audio {
    var id: Int
    var name: String
    var file: String
    var duration: Double
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.file = json["file"].stringValue
        self.duration = json["duration"].doubleValue
    }
  
    
}

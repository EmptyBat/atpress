//
//  News.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 31.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct News{
    
    let id: Int
    let name: String
    let image: String
    let published_at: String
    let desc: String
    let viewCount: Int
    var categoryName: String
    var isFavorite: Bool
    var share_link: String
    
    init(json: JSON) {
        
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        let image = json["image"]["thumb"].stringValue
        self.image = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.desc = json["description"].stringValue
        self.viewCount =  json["hits"].intValue
        self.categoryName = json["category"]["name"].stringValue
        self.published_at = json["published_at"].intValue.convertToDate()
        self.isFavorite = json["favorite"].boolValue
        self.share_link = json["share_link"].stringValue
    }
}

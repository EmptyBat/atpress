//
//  Baner.swift
//  ernur.kz
//
//  Created by Admin on 17.06.2018.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Baner {
    
    var name: String
    var section_name: String
    var position: String
    var url: String
    
    
    init(json: JSON) {
        self.name = json["name"].stringValue
        self.section_name = json["section_name"].stringValue
        self.position = json["position"].stringValue
        self.url = json["url"].stringValue
    }
    
}


//
//  RealmNews.swift
//  egemen.kz
//
//  Created by Toremurat on 27.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import RealmSwift

class RealmNews: Object{
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var text = ""
    dynamic var data = ""
    dynamic var image = ""
    dynamic var viewCount = 0
    dynamic var commentCount = 0
    dynamic var categoryName = ""
  
    override class func primaryKey() -> String? {
        return "id"
    }
    
}

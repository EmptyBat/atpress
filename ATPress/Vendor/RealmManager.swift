//
//  RealmManager.swift
//  egemen.kz
//
//  Created by Toremurat on 27.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SVProgressHUD

class RealmManager {

    let realm = try! Realm()
    
    func deleteObject(object: Object) {
        try! realm.write({
            realm.delete(object)
            SVProgressHUD.showSuccess(withStatus: "Таңдаулы жаңалықтардаң өшірілді")
        })
    }
    
    func deleteDatabase() {
        try! realm.write({
            realm.deleteAll()
        })
    }
    func saveObjects(objs: [Object]) {
        try! realm.write({
            realm.add(objs, update: true)
            SVProgressHUD.showSuccess(withStatus: "Таңдаулы жаңалықтарға сақталды")
        })
    }
    
    func getObjects(type: Object.Type) -> Results<Object>? {
        return realm.objects(type)
    }
    
    // MARK: Realm Saving
    func saveArticle(news: News) {
        let obj = RealmNews(value: ["id": news.id, "name": news.name, "text": "", "categoryName": news.categoryName, "image": news.image, "commentCount": 0, "viewCount": news.viewCount, "data": news.published_at])
        saveObjects(objs: [obj])
    }
    
    func removeArticleById(_ id: Int) {
        let obj = realm.objects(RealmNews.self).filter("id = \(id)").first
        if let wrapObj = obj {
            deleteObject(object: wrapObj)
        }
    }

}

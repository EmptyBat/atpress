//
//  AuthManager.swift
//  ernur.kz
//
//  Created by Toremurat on 12.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation

struct AuthManager {
    
    static let shared: AuthManager = {
        let instance = AuthManager()
        return instance
    }()
    
    func logout(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
        UserDefaults.standard.removeObject(forKey: "UserId")
        UserDefaults.standard.synchronize()
    }
    
    func login(_ user_id: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
        UserDefaults.standard.setValue(user_id, forKey: "UserId")
        UserDefaults.standard.synchronize()
    }
    
}

//
//  MenuRubricTableViewCell.swift
//  infoBoom
//
//  Created by Toremurat on 03.01.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import SnapKit

class MenuTableViewCell: UITableViewCell {
    
    var title: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "Lato-Regular", size: 20.0)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(title)
        title.snp.makeConstraints { (make) in
            make.left.equalTo(40.0)
            make.centerY.equalToSuperview()
            make.right.equalTo(-32.0)
        }
    }
    
    func makeTitleBold() {
         self.title.font = UIFont.init(name: "Lato-Bold", size: 20.0)
    }
    
    func setupData(_ name: String) {
        self.title.text = name
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


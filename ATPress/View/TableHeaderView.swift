//
//  TableHeaderSectionView.swift
//  ernur.kz
//
//  Created by Toremurat on 27.05.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import UIKit

class TableHeaderView: UIView {
  
    var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.font = UIFont.init(name: "Lato-Bold", size: 18.0)
        headerLabel.text = ""
        headerLabel.sizeToFit()
        return headerLabel
    }()
    
    let lineView: UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor(white: 0.0, alpha: 0.07)
        return lineView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        addSubview(headerLabel)
        addSubview(lineView)
        setupConstaints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstaints() {
        
        lineView.snp.makeConstraints { (make) in
            make.left.equalTo(0.0)
            make.top.equalTo(0.0)
            make.right.equalTo(0.0)
            make.height.equalTo(8.0)
        }
        
        headerLabel.snp.makeConstraints { (make) in
            make.left.equalTo(8.0)
            make.top.equalTo(lineView.snp.bottom).offset(16.0)
            make.right.equalTo(-8.0)
            make.bottom.equalToSuperview().offset(-8.0)
        }
    }
}

//
//  CommentTableViewCell.swift
//  egemen.kz
//
//  Created by Toremurat on 28.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgAva: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgAva.layer.cornerRadius = imgAva.frame.size.width/2
    }
    
    func setupData(_ comment: Comment) {
        //imgAva.sd_setImage(with: URL(string: comment.avatar))
        nameLabel.text = comment.name
        contentLabel.text = comment.text
        dateLabel.text = comment.date.convertToDate()
    }

}

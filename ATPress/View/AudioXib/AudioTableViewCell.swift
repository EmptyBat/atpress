//
//  AudioTableViewCell.swift
//  ernur.kz
//
//  Created by Toremurat on 02.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
protocol AudioCellDelegate {
    func changePlayIcon(_ image: String)
}
class AudioTableViewCell: UITableViewCell, AudioCellDelegate {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupData(_ audio: Audio) {
        titleLabel.text = audio.name
        let min = audio.duration/60
        durationLabel.text = "\(Int(min))" + ":" + "\(Int(audio.duration) % 60)"
//        iconImage.image = UIImage(named: audio.isPlaying ? "pause" : "play")
        
    }
    func changePlayIcon(_ image: String) {
        iconImage.image = UIImage(named: image)
    }
}

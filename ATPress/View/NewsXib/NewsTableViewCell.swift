//
//  NewsTableViewCell.swift
//  ernur.kz
//
//  Created by Toremurat on 31.03.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var favButton: UIButton!
    
    @IBOutlet weak var viewsIcon: UIImageView!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgImage: UIImageView!

    var isFavorited: Bool = false
    var addCompletion : addFavoriteBlock! = nil
    var removeCompletion : removeFavoriteBlock! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        viewsIcon.layer.opacity = 0.5
    }

    @IBAction func favButtonPressed(_ sender: UIButton) {
        if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int {
            let params = ["user_id" : userId, "article_id": favButton.tag]
            if isFavorited {
                NetworkManager.makeRequest(target: .removeFavorite(params), success: { (json) in
                    self.favButton.isSelected = false
                })
            } else {
                NetworkManager.makeRequest(target: .addFavorite(params), success: { (json) in
                    self.favButton.isSelected = true
                })
            }
        } else {
            if favButton.isSelected {
                favButton.isSelected = false
                removeCompletion!()
                print("need to remove")
            } else {
                favButton.isSelected = true
                addCompletion!()
                print("need to add")
            }
        }
    }
    
    func setupData(_ news: News) {
        bgImage.sd_setImage(with: URL(string: news.image))
        titleLabel.text = news.name
        viewsLabel.text = "\(news.viewCount)"
        dateLabel.text = news.published_at
        isFavorited = news.isFavorite
        
        favButton.setImage(UIImage(named: "icon_fav_red"), for: .selected)
        favButton.setImage(UIImage(named: "icon_fav_gr"), for: .normal)
        favButton.isSelected = news.isFavorite
    }
}

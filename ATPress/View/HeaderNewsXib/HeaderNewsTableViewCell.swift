//
//  NewsTableViewCell.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 25.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit

class HeaderNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.backgroundColor = UIColor.gray.cgColor
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(8, 8, 0, 8))
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = 3.0
    }
    
    func setupData(_ news: News) {
        titleLabel.text = news.name
        bgImage.sd_setImage(with: URL(string: news.image))
        viewsLabel.text = "\(news.viewCount)"
        dateLabel.text = news.published_at
    }
}

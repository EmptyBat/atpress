//
//  PhotoIndexCell.swift
//  TopMaster
//
//  Created by Narikbi on 28.11.17.
//  Copyright © 2017 BuginGroup. All rights reserved.
//

import Foundation

class PhotoIndexCell : UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var roundedContainerView: UIView!
    @IBOutlet weak var numberLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configUI()
    }
    
    func setCellIndex(_ index : Int) {
        numberLabel.text = "\(index)"
    }
    
    func configUI() {
        self.clipsToBounds = true

        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true
        containerView.layer.borderColor = UIColor.black.cgColor
        containerView.layer.borderWidth = 0.4
        
        roundedContainerView.layer.cornerRadius = 16
        roundedContainerView.layer.masksToBounds = true
        
    }
    
    

}

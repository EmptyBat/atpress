//
//  PhotoAddCell.swift
//  TopMaster
//
//  Created by Narikbi on 23.11.17.
//  Copyright © 2017 BuginGroup. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

typealias ActionBlock = () -> Void


class PhotoAddCell : UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var deleteBlock : ActionBlock?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
        
        self.configUI()
        
    }

    func setPhoto(url : String) {
        imageView.sd_setImage(with: URL(string: url))
    }
    
	// MARK: Actions

    @IBAction func deleteTapped(_ sender: Any) {
        deleteBlock!()
    }
    
    func setBorderSelected(selected : Bool) {
        if selected {
            containerView.layer.borderColor = BaseMessage.appColor.cgColor
            containerView.layer.borderWidth = 2
        } else {
            containerView.layer.borderColor = BaseMessage.appColor.cgColor
            containerView.layer.borderWidth = 0.4
        }

    }
    
    
    func configUI() {
        self.clipsToBounds = true
        
        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true
        containerView.layer.borderColor = BaseMessage.appColor.cgColor
        containerView.layer.borderWidth = 0.4
        
    }

}


//
//  PhotoAddActionCell.swift
//  TopMaster
//
//  Created by Narikbi on 28.11.17.
//  Copyright © 2017 BuginGroup. All rights reserved.
//

import Foundation

class PhotoAddActionCell : UICollectionViewCell{
    
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.configUI()
    }
    
    func configUI() {
        self.clipsToBounds = true
        
        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true
        containerView.layer.borderColor = BaseMessage.appColor.cgColor
        containerView.layer.borderWidth = 0.4
    }
    
}

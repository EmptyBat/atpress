//
//  YandexAdsCell.swift
//  ernur.kz
//
//  Created by Toremurat on 08.05.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

/*import UIKit
import YandexMobileAds

class YandexAdsCell: UITableViewCell {
    //MARK: YandexAds
    var adLoader: YMANativeAdLoader!
    var bannerView: YMANativeBannerView!
    var bannerId: Int = 1
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        bannerView = YMANativeBannerView(frame: CGRect.zero)
        addSubview(bannerView)
        bannerView.snp.makeConstraints { (make) in
            make.left.equalTo(8.0)
            make.top.equalTo(0.0)
            make.right.equalTo(0.0)
            make.bottom.equalTo(0.0)
        }
        print("R-M-273302-\(bannerId)")
        let configuration = YMANativeAdLoaderConfiguration(blockID: "R-M-273302-\(bannerId)",
                                                           imageSizes: [kYMANativeImageSizeMedium],
                                                           loadImagesAutomatically: true)
        self.adLoader = YMANativeAdLoader(configuration: configuration)
        self.adLoader.delegate = self
        self.adLoader.loadAd(with: nil)
    }
    
    func loadAds() {
        self.adLoader.loadAd(with: nil)
    }
    
    func didLoadAd(_ ad: YMANativeGenericAd) {
        ad.delegate = self
        bannerView.ad = ad
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension YandexAdsCell: YMANativeAdDelegate, YMANativeAdLoaderDelegate {
    // MARK: - YMANativeAdDelegate
    
    func nativeAdLoader(_ loader: YMANativeAdLoader!, didLoad ad: YMANativeAppInstallAd) {
        print("Loaded App Install ad")
        didLoadAd(ad)
    }
    
    func nativeAdLoader(_ loader: YMANativeAdLoader!, didLoad ad: YMANativeContentAd) {
        print("Loaded Content ad")
        didLoadAd(ad)
    }
    
    func nativeAdLoader(_ loader: YMANativeAdLoader!, didFailLoadingWithError error: Error) {
        print("Native ad loading error: \(error)")
    }
    
    // MARK: - YMANativeAdLoaderDelegate
    
    // Uncomment to open web links in in-app browser
    //    func viewControllerForPresentingModalView() -> UIViewController {
    //        return self
    //    }
    
    func nativeAdWillLeaveApplication(_ ad: Any!) {
        print("Will leave application")
    }
    
    func nativeAd(_ ad: Any!, willPresentScreen viewController: UIViewController) {
        print("Will present screen")
    }
    
    func nativeAd(_ ad: Any!, didDismissScreen viewController: UIViewController) {
        print("Did dismiss screen")
    }
}
*/

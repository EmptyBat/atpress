//
//  CurrencyTableViewCell.swift
//  infoBoom
//
//  Created by Toremurat on 18.02.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {


    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

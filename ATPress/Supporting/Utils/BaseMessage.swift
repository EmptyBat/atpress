//
//  BaseUrls.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 31.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation

struct BaseMessage {
    static var baseUrl = "http://atpress.bugingroup.com"
    static let shareLink = "Подробнее на http://atpress.kz"
    
   // static let appColor = UIColor(red:0.49, green:0.19, blue:0.89, alpha:1.0)
    static let appColor = UIColor.white
    
    static let passwordError = "Құпия сөздер сәйкес келмейді"
    static let parsingError = "Деректерді жүктеу қатесі"
    static let fillError = "Қажетті ақпаратты толтырыңыз"
    static let commentAdded = "Комментарий отправлен на модерацию"
}

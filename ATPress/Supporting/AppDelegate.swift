//
//  AppDelegate.swift
//  infoBoom
//
//  Created by Toremurat on 02.01.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import UserNotifications
import SVProgressHUD
import SwiftyJSON
import Localize_Swift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configBasicPreferences()
        registerPushNotifications()
        if (Localize.currentLanguage() != "ru") || (Localize.currentLanguage() != "kk-KZ")
        {
            Localize.setCurrentLanguage("ru")
        }
        
        return true
    }
    func configBasicPreferences() {
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setForegroundColor(BaseMessage.appColor)
        SVProgressHUD.setRingNoTextRadius(15.0)
    }
    
    func registerPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                print("Permission granted: \(granted)")
                guard granted else { return }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
   
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        registerDeviceToken(pushToken: token)
        print("Device Token: \(token)")
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
   
    func registerDeviceToken(pushToken:String) {
        
        if let savedToken = UserDefaults.standard.value(forKey: "UserPushToken") as? String {
            if savedToken == pushToken {
                return
            }
        }
        let params = ["os": "IOS", "registration_id": pushToken]
        NetworkManager.makeRequest(target: .addtoken(params)) { (json) in
            UserDefaults.standard.set(pushToken, forKey: "UserPushToken")
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
      
      /*  guard
            let aps = userInfo[AnyHashable("extra")] as? NSDictionary
            else {
                return
        }
        let news_id = aps["id"] as? Int
        let notificationName = Notification.Name("push")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            NotificationCenter.default.post(name: notificationName, object: news_id)
        }*/
       // completionHandler()
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        //Handle the notification
        guard
            let aps = notification.request.content.userInfo[AnyHashable("extra")] as? NSDictionary
            else {
                return
        }
        let news_id = aps["id"] as? Int
        let lang = aps["lang"] as? String
        if (lang == "ru"){
            Localize.setCurrentLanguage("ru")
        }
        else {
            Localize.setCurrentLanguage("kk-KZ")
        }
        let notificationName = Notification.Name("push")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            NotificationCenter.default.post(name: notificationName, object: news_id)
        }
        print("working here 12345")
         completionHandler([.alert, .badge, .sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
        guard
            let aps = response.notification.request.content.userInfo[AnyHashable("extra")] as? NSDictionary
            else {
                return
        }
        let news_id = aps["id"] as? Int
        let lang = aps["lang"] as? String
        if (lang == "ru"){
            Localize.setCurrentLanguage("ru")
        }
        else {
            Localize.setCurrentLanguage("kk-KZ")
        }
        let notificationName = Notification.Name("push")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            NotificationCenter.default.post(name: notificationName, object: news_id)
        }
        print("working here 123")
        completionHandler()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard
            let aps = userInfo[AnyHashable("extra")] as? NSDictionary
            else {
                return
        }
        let news_id = aps["id"] as? Int
        let lang = aps["lang"] as? String
        if (lang == "ru"){
            Localize.setCurrentLanguage("ru")
        }
        else {
            Localize.setCurrentLanguage("kk-KZ")
        }
        let notificationName = Notification.Name("push")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            NotificationCenter.default.post(name: notificationName, object: news_id)
        }
        print("working here 123")
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        
        NSLog("we are in the background...")
    }
    
    func applicationWillTerminate(application: UIApplication) {
        
        NSLog("we have terminated")
    }


}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard
            let aps = notification.request.content.userInfo[AnyHashable("extra")] as? NSDictionary
            else {
                return
        }
        let news_id = aps["id"] as? Int
        let lang = aps["lang"] as? String
        if (lang == "ru"){
            Localize.setCurrentLanguage("ru")
        }
        else {
            Localize.setCurrentLanguage("kk-KZ")
        }
        let notificationName = Notification.Name("push")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            NotificationCenter.default.post(name: notificationName, object: news_id)
        }
        print("working here 123",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        guard
            let aps = response.notification.request.content.userInfo[AnyHashable("extra")] as? NSDictionary
            else {
                return
        }
        let news_id = aps["id"] as? Int
        let lang = aps["lang"] as? String
        if (lang == "ru"){
            Localize.setCurrentLanguage("ru")
        }
        else {
            Localize.setCurrentLanguage("kk-KZ")
        }
        let notificationName = Notification.Name("push")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            NotificationCenter.default.post(name: notificationName, object: news_id)
        }
        print("working here 123",response.notification.request.content.userInfo)
        completionHandler()
    }
    
}

//
//  ViewController+Extension.swift
//  egemen.kz
//
//  Created by Toremurat on 19.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation

// MARK: UIViewController Extension

extension UIViewController {
    
    func setEmptyBackTitle() {
        self.navigationController?.navigationBar.tintColor = UIColor(red:0/255, green:86/255, blue:142/255, alpha:1.0)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black
    }
    func setClearNavbar(Navigation:UINavigationController){
        Navigation.navigationBar.tintColor = UIColor(red:0/255, green:86/255, blue:142/255, alpha:1.0)
        Navigation.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName : UIFont.init(name: "Lato-Bold", size: 18.0)]
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

// MARK: String Extensions

extension String {
    
    var html2AttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func converDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = dateFormatter.date(from: self) else {
            return ""
        }
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: date)
    }

    func contentsOrBlank()->String {
        if let path = Bundle.main.path(forResource: self, ofType: nil) {
            do {
                let text = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                return text
            } catch { print("Failed to read text from bundle file \(self)") }
        } else { print("Failed to load file from bundle \(self)") }
        return ""
    }
}

// MARK: Int Extension

extension Int {
    
    func convertToDate() -> String {
        let formatter = DateFormatter()
        let date =  Date(timeIntervalSince1970: TimeInterval(self))
        
        if Calendar.current.isDateInToday(date) {
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: date)
            return "today, ".localized() + ", " + dateString
        } else if Calendar.current.isDateInYesterday(date) {
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: date)
            return "yesterday, ".localized() + ", " + dateString
        } else {
             formatter.dateFormat = "dd.MM.yy HH:mm"
            let dateString = formatter.string(from: date)
            return dateString
        }
    }
    
}

// MARK: UIView Extension
extension UIView  {
    
    func makeSnapshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}



//
//  ReportContactsController.swift
//  ernur.kz
//
//  Created by Toremurat on 12.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SVProgressHUD
import InputMask
import Localize_Swift
class ReportContactsController: UIViewController, MaskedTextFieldDelegateListener {
    @IBOutlet weak var title_text: UILabel!
    
    @IBOutlet weak var write_btn: UIButton!
    @IBOutlet weak var phoneField: SkyFloatingLabelTextField!
    @IBOutlet weak var nameField: SkyFloatingLabelTextField!
    
    var maskedDelegate: MaskedTextFieldDelegate?
    var isValidPhone = false
    var phoneNumber = ""
    
    static func instantiate() -> ReportContactsController {
        return UIStoryboard.init(name: "Audio", bundle: nil).instantiateViewController(withIdentifier: "ReportContactsController") as! ReportContactsController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Feedback".localized()
        setEmptyBackTitle()
        self.hideKeyboardWhenTappedAround()
        maskedDelegate = MaskedTextFieldDelegate(format: "{+7} ([000]) [000] [00] [00]")
        maskedDelegate?.listener = self
        phoneField.delegate = maskedDelegate
        settext()
    }
    func settext() {
       title_text.text = "desk".localized()
       phoneField.placeholder = "callback".localized()
       nameField.placeholder = "name".localized()
      write_btn.setTitle("write".localized(), for: .normal)
    }
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        guard let name = nameField.text, !name.isEmpty, let phone = phoneField.text, !phone.isEmpty else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
        }
        if isValidPhone {
            let vc = ReportController.instantiate()
            vc.phone = phoneNumber
            vc.name = name
            self.show(vc, sender: nil)
        } else {
            SVProgressHUD.showError(withStatus: "callbackwrong".localized())
        }
       
    }
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        phoneNumber = value
        isValidPhone = complete
    }
    
}

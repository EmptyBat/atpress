//
//  ReportViewModel.swift
//  ernur.kz
//
//  Created by Toremurat on 11.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SVProgressHUD

class ReportViewModel : NSObject {
    var images: [UIImage] = []
    var photosUrl: [String] = []
    var event_name:String = ""
    var event_date = ""
    var event_address = ""
    var name: String = ""
    var phone: String = ""
    
    override init() {
        super.init()
    }
    
    func uploadImage(image : UIImage, esca completion: @escaping () -> Void ) {
     /*   let data = UIImageJPEGRepresentation(image, 0.5)!
        
        NetworkManager.makeRequest(target: .uploadReportImage(data)) {[weak self] (json) in
            guard let vc = self else { return }
            if let fileName = json["data"]["fileName"].string {
                vc.images.append(image)
                vc.photosUrl.append(fileName)
                completion()
            } else {
                SVProgressHUD.showError(withStatus: "Не получилось загрузить изображение.. Попробуйте позже")
            }
            print(json)
        }*/
        images.append(image)
        //photosUrl.append(fileName)
        completion()
    }
    
    func prepareFileNames() -> String {
        // Format <file1>,<file2>, etc..
        var files = ""
        for name in photosUrl {
            if name == photosUrl.last {
                files += name
            } else {
                files += name + ","
            }
        }
        return files
    }
    
    func dictValue() -> [String:Any] {
        
        var params = [String:Any]()
        params["name"] = self.name
        params["phone"] = self.phone
        params["files"] = self.photosUrl
        params["event_name"] = self.event_name
        params["event_date"] = self.event_date
        params["event_address"] = self.event_address
        
        return params
    }
    
    
}

//
//  ReportController.swift
//  ernur.kz
//
//  Created by Toremurat on 10.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import CropViewController
import SkyFloatingLabelTextField
import SVProgressHUD
import Localize_Swift
class ReportController: UITableViewController {
    @IBOutlet weak var send_btn: UIButton!
    
    @IBOutlet weak var title_attach: UILabel!
    @IBOutlet weak var title_text: UILabel!
    let viewModel = ReportViewModel()
    var name: String = ""
    var phone: String = ""
    var report_image : UIImage?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descField: SkyFloatingLabelTextField!
    @IBOutlet weak var timeField: SkyFloatingLabelTextField!
    @IBOutlet weak var placeField: SkyFloatingLabelTextField!
    
    static func instantiate() -> ReportController {
        return UIStoryboard.init(name: "Audio", bundle: nil).instantiateViewController(withIdentifier: "ReportController") as! ReportController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        settext()
    }
    func settext() {
        send_btn.setTitle("send".localized(), for: .normal)
        title_attach.text = "attach".localized()
        title_attach.text = "attach".localized()
        title_text.text = "title_text".localized()
        descField.placeholder = "desk".localized()
        timeField.placeholder = "time".localized()
        placeField.placeholder = "place".localized()
    }
    
    func configUI(){
        self.title = "Reporter".localized()
        self.collectionView.backgroundColor = .white
        //self.hideKeyboardWhenTappedAround()
    
        self.collectionView.register(UINib.init(nibName: "PhotoAddCell", bundle: nil), forCellWithReuseIdentifier: "PhotoAddCell")
        self.collectionView.register(UINib.init(nibName: "PhotoAddActionCell", bundle: nil), forCellWithReuseIdentifier: "PhotoAddActionCell")
        self.collectionView.register(UINib.init(nibName: "PhotoIndexCell", bundle: nil), forCellWithReuseIdentifier: "PhotoIndexCell")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    func savePressed() -> Void {
        print(viewModel.photosUrl)
        print(viewModel.prepareFileNames())
        let data = UIImageJPEGRepresentation(self.report_image!, 0.5)!
        
        NetworkManager.makeRequest(target: .uploadReportImage(data,user_name : name , email: "robot@mail.ru", phone: phone)) {[weak self] (json) in
            guard let vc = self else { return }
            SVProgressHUD.showSuccess(withStatus: "Ваш репорт успешно отправлен!")
            vc.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    func addPhotoTapped() {
        let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
        let firstAction = UIAlertAction(title: "Выбрать фото", style: .default) { (_) in
            self.openPicker()
        }
        let secondAction = UIAlertAction(title: "Снять фото", style: .default) { (_) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openPicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isCameraDeviceAvailable(.front) || UIImagePickerController.isCameraDeviceAvailable(.rear) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            SVProgressHUD.showError(withStatus: "Құрылғыңыз камераны қолдамайды")
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 5 {
            savePressed()
        }
    }
   
}
extension ReportController : UICollectionViewDataSource {
    
    // MARK: CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoAddActionCell", for: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoAddCell", for: indexPath) as! PhotoAddCell
    
            cell.imageView.image = report_image
            return cell
        }
    }
    
}

extension ReportController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item < viewModel.photosUrl.count {
            print("tapped")
        } else {
            // calls action sheet to pick photo
            addPhotoTapped()
        }
    }
    
}

extension ReportController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 70, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
    }
    
}

extension ReportController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true) {
            self.cropPickedImage(image)
        }
    }
    
    func cropPickedImage(_ photo : UIImage) {
        
        let cropViewController = CropViewController(image: photo)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
}

extension ReportController : CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true)  {
               self.report_image = image
               self.collectionView.reloadData()
           
        }
    }
}


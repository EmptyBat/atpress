//
//  MusicPlayerController.swift
//  ernur.kz
//
//  Created by Toremurat on 05.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

/*import UIKit
import AVFoundation
import LNPopupController
import SVProgressHUD


class MusicPlayerController: UIViewController {

    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var playButton: UIButton!
    var miniPause: UIBarButtonItem!
    var miniNext: UIBarButtonItem!
    var delegate: AudioDelegate?
    
    var totalDuration: Double = 0.0
    let accessibilityDateComponentsFormatter = DateComponentsFormatter()
    var audios: [Audio] = []
    var isPause: Bool = false
    
    // Player items
    var player: AVPlayer?
    var timer : Timer?
    
    var currentAudioIndex: Int = 0 {
        didSet {
            if currentAudioIndex < 0 {
                currentAudioIndex = 0
            } else if currentAudioIndex >= audios.count {
                currentAudioIndex = 0
            }
        }
    }
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
   
        miniPause = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(pausePlayButtonPressed))
        miniNext = UIBarButtonItem(image: UIImage(named: "nextFwd"), style: .plain, target: self, action: #selector(fwdButtonPressed))
        popupItem.rightBarButtonItems = [ miniPause, miniNext ]
    }
    
    var songTitle: String = "" {
        didSet {
            if isViewLoaded {
                songNameLabel.text = songTitle
            }
            popupItem.title = songTitle
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        songNameLabel.text = audios[currentAudioIndex].name
        popupItem.title = audios[currentAudioIndex].name
       
        preparePlayer(audioUrl: audios[currentAudioIndex].file)
        NotificationCenter.default.addObserver(self, selector: #selector(stopStreamingAudio), name: NSNotification.Name.init("StopAudioStreaming"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(beginStreming), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
    }
    
    // MARK: Audio ready to play
    func beginStreming() {
        
    }
    // MARK: Notification StopAudioStreaming function
    func stopStreamingAudio() {
        print("stoppeee")
        player?.pause()
        player = nil
        stopTimer()
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    // MARK: Updating data
    func updateLabels() {
        songNameLabel.text = audios[currentAudioIndex].name
        popupItem.title = audios[currentAudioIndex].name
        totalDuration = audios[currentAudioIndex].duration
        
    }
    
    // MARK: Prepare audio to play
    func preparePlayer(audioUrl: String) {
       // player = nil
        stopTimer()
        let totalTime = Int(totalDuration)
        endLabel.text = "\(totalTime/60):\(totalTime%60)"
        if let url = URL(string: audios[currentAudioIndex].file) {
            let item = AVPlayerItem(url: url)
            totalDuration = audios[currentAudioIndex].duration
            player = AVPlayer(playerItem: item)
            player?.play()
            timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(MusicPlayerController._timerTicked(_:)), userInfo: nil, repeats: true)
        }
        updateLabels()
    }
    
    
    // MARK: IBActions Pause, Back, Fwd
    @IBAction func pausePlayButtonPressed(_ sender: UIButton) {
        
        if player?.rate == 0 {
            player?.play()
            playButton.setImage(UIImage(named: "nowPlaying_pause"), for: .normal)
            miniPause.image = UIImage(named: "pause")
            timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(MusicPlayerController._timerTicked(_:)), userInfo: nil, repeats: true)
            
        } else {
            player?.pause()
            playButton.setImage(UIImage(named: "nowPlaying_play"), for: .normal)
            miniPause.image = UIImage(named: "play")
            timer?.invalidate()
        }
        delegate?.updateCells(currentAudioIndex)
    }
    
    @IBAction func bwdButtonPressed(_ sender: UIButton) {
        currentAudioIndex -= 1
        preparePlayer(audioUrl: audios[currentAudioIndex].file)
        popupItem.progress = 0.0
        delegate?.updateCells(currentAudioIndex)
    }
    
    @IBAction func fwdButtonPressed(_ sender: UIButton) {
        currentAudioIndex += 1
        preparePlayer(audioUrl: audios[currentAudioIndex].file)
        popupItem.progress = 0.0
        delegate?.updateCells(currentAudioIndex)
    }
    
    // MARK: Timer function
    @objc func _timerTicked(_ timer: Timer) {
       
        let progrressStep:Float = Float((1/TimeInterval(totalDuration))/20)
        popupItem.progress += progrressStep
        progressView.progress = popupItem.progress
        
        startLabel.text = "0:00"
        print(popupItem.progress)
        if popupItem.progress >= 1.0 {
            timer.invalidate()
            //popupPresentationContainer?.dismissPopupBar(animated: true, completion: nil)
            currentAudioIndex += 1
            popupItem.progress = 0.0
            preparePlayer(audioUrl: audios[currentAudioIndex].file)
            delegate?.updateCells(currentAudioIndex)
        }
    }
}
*/

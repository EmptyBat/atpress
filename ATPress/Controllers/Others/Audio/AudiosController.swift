//
//  AudiosController.swift
//  ernur.kz
//
//  Created by Toremurat on 02.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

/*import UIKit
import SwiftyJSON
import AVFoundation

protocol AudioDelegate {
    func updateCells(_ currentIndex: Int)
}

class AudiosController: UIViewController, AudioDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    // Variables
    var audios: [Audio] = []
    var playingIndex = 0
    
    static func instantiate() -> AudiosController {
        return UIStoryboard.init(name: "Audio", bundle: nil).instantiateViewController(withIdentifier: "AudiosController") as! AudiosController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Аудио"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioCell")
       
        self.popupBar.tintColor = UIColor(white: 38.0 / 255.0, alpha: 1.0)
        self.popupBar.imageView.layer.cornerRadius = 5
        self.popupBar.progressViewStyle = .top
        
        loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopAudioStreaming"), object: nil)
    }
    // MARK: Load Data
    func loadData() {
        NetworkManager.makeRequest(target: .getAudios()) { [weak self] (json) in
            guard let vc = self else { return }
            for (_, subJson):(String, JSON) in json["data"]["data"] {
                vc.audios.append(Audio.init(json: subJson))
            }
            vc.tableView.reloadData()
            let indexPath = IndexPath(row: 0, section: 0)
            vc.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
            vc.tableView.delegate?.tableView!(vc.tableView, didSelectRowAt: indexPath)
        }
    }
    
    // MARK: AudioDelegate function
    func updateCells(_ currentIndex: Int) {
        playingIndex = currentIndex
        tableView.reloadData()
    }
    
}

// MARK: UITableView Delegate/DatSource
extension AudiosController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioCell", for: indexPath) as! AudioTableViewCell
        cell.iconImage.image = UIImage(named: indexPath.row == playingIndex ? "pause" : "play")
        cell.setupData(audios[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        playingIndex = indexPath.row
        tableView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopAudioStreaming"), object: nil)
        let vc = UIStoryboard.init(name: "Audio", bundle: nil).instantiateViewController(withIdentifier: "MusicPlayerController") as! MusicPlayerController
        
        vc.songTitle = audios[indexPath.row].name
        vc.audios = audios
        vc.currentAudioIndex = indexPath.row
        vc.totalDuration = audios[indexPath.row].duration
        vc.delegate = self
        
        self.presentPopupBar(withContentViewController: vc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AudioTableViewCell
        cell.changePlayIcon("play")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}
*/

//
//  CurrencyController.swift
//  infoBoom
//
//  Created by Toremurat on 18.02.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import SwiftyJSON

class CurrencyController: UITableViewController {

    var data: [Currency] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        configureUI()
        loadData()
    }
    
    // MARK: Configuring UI elements
    func configureUI() {
        tableView.register(UINib.init(nibName: "CurrencyTableViewCell", bundle: nil), forCellReuseIdentifier: "CurrencyCell")
        tableView.separatorStyle = .none
    }
    
    // MARK: LoadData
    func loadData() {
        NetworkManager.makeRequest(target: .getCurrency()) { [weak self] (json) in
               print ("tested",json["data"])
            guard let vc = self else { return }
            for (_, subJson):(String, JSON) in json["data"] {
                vc.data.append(Currency.init(json: subJson))
            }
            vc.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }   

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath) as! CurrencyTableViewCell
        cell.title.text = data[indexPath.row].title
        cell.amount.text = "\(data[indexPath.row].amount) тг"
        cell.subtitle.text = data[indexPath.row].desc
        cell.date.text = data[indexPath.row].pubDate
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
   

}

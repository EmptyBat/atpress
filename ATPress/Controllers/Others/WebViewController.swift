//
//  WebViewController.swift
//  ernur.kz
//
//  Created by Toremurat on 13.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var webView: UIWebView!
    var titleName:String = ""
    var urlString: String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = titleName
        webView.delegate = self
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if let url = URL(string: urlString) {
             webView.loadRequest(URLRequest(url: url))
        }
        if let navCtrl = self.navigationController {
            setClearNavbar(Navigation: navCtrl)
        }
  webView.dataDetectorTypes.remove(UIDataDetectorTypes.all)
        webView.isUserInteractionEnabled = false
 
       
    }
    func webView(WebViewNews: UIWebView!, shouldStartLoadWithRequest request: NSURLRequest!, navigationType: UIWebViewNavigationType) -> Bool {
        print ("webviewCalling flase")
        return false;
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let scrollPoint = CGPoint(x: 0, y: 200)
        webView.scrollView.setContentOffset(scrollPoint, animated: true)
        let script = "document.documentElement.style.webkitUserSelect='none'"
        if let returnedString = webView.stringByEvaluatingJavaScript(from :script) {
            print("the result is \(returnedString)")
        }
        let script1 = "document.documentElement.style.webkitTouchCallout='none'"
        if let returnedString1 = webView.stringByEvaluatingJavaScript(from :script1) {
            print("the result is \(returnedString1)")
        }
    }
}

//
//  SettingsTableViewController.swift
//  egemen.kz
//
//  Created by Toremurat on 25.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Localize_Swift
class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var lang_title: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setText()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        tableView.tableFooterView = UIView()
        if let navCtrl = self.navigationController {
            setClearNavbar(Navigation: navCtrl)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: Switch Action
    @objc func setText(){
        self.title = "settings".localized()
        lang_title.text = "title_setting".localized()
    }
    @IBAction func switchAction(_ sender: UISwitch) {
        if sender.isOn {
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            UIApplication.shared.unregisterForRemoteNotifications()
        }
    }
    
    // MARK: TableView Delegates
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func createAlert() {
        let alertView = UIAlertController(title: "", message: "alert".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 1){
            let actionSheetController = UIAlertController(title: "title_setting".localized(), message: "", preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                print("Отмена")
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton = UIAlertAction(title: "Русский язык", style: .default) { action -> Void in
                print("Русский")
                Localize.setCurrentLanguage("ru")
            }
            actionSheetController.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "Қазақ тілі", style: .default) { action -> Void in
                print("Қазақ тілі")
                Localize.setCurrentLanguage("kk-KZ")
            }
            actionSheetController.addAction(deleteActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }
}
    
}

//
//  FavoriteTableViewController.swift
//  egemen.kz
//
//  Created by Toremurat on 26.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class FavoriteTableViewController: UITableViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var rehreshC: UIRefreshControl!
    
    // MARK: Variables
    
    let realm = RealmManager()
    var news = List<RealmNews>()
    var favorites: [News] = []
    var isLogged: Bool = false
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "favorite".localized()
        setEmptyBackTitle()
        if let navCtrl = self.navigationController {
            setClearNavbar(Navigation: navCtrl)
        }
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
  
        tableView.registerNib(NewsTableViewCell.self)
        tableView.tableFooterView = UIView()
        
        rehreshC = UIRefreshControl()
        rehreshC.addTarget(self, action: #selector(refreshToogle), for: .valueChanged)
        rehreshC.attributedTitle = NSAttributedString(string: "update".localized())
        tableView.addSubview(rehreshC)
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.editButtonItem.tintColor = UIColor.white
        
        loadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setEditing(false, animated: true)
    }
    
    // MARK: Load data from Server
    
    func loadData() {
        if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int {
            NetworkManager.makeRequest(target: .getFavorites(userId: userId), success: { [weak self] (json) in
                guard let vc = self else { return }
                vc.favorites.removeAll()
                for (_, subJson):(String, JSON) in json["data"]["items"] {
                    let new = News(json: subJson)
                    vc.favorites.append(new)
                }
                vc.isLogged = true
                vc.tableView.reloadData()
            })
        } else {
            isLogged = false
            getRealmObject()
        }
    }
    
    // MARK: Load data from Realm
    
    func getRealmObject() {
        if let objects = realm.getObjects(type: RealmNews.self) {
            news.removeAll()
            for element in objects {
                if let new = element as? RealmNews {
                   news.append(new)
                }
            }
            tableView.reloadData()
        }
    }
    
    // MARK: Refreshing action
    
    func refreshToogle() {
        loadData()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            self.rehreshC.endRefreshing()
        }
    }
    
    // Editing action
    
    func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.reloadData()
    }
    
}

// MARK: TableView Delegate & DataSource
extension FavoriteTableViewController {
 
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isLogged ? favorites.count : news.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NewsTableViewCell = tableView.dequeReusableCell(for: indexPath)
        cell.selectionStyle = .none
        if isLogged {
            cell.bgImage.sd_setImage(with: URL(string: favorites[indexPath.row].image))
            cell.titleLabel.text = favorites[indexPath.row].name
            cell.viewsLabel.text = "\(favorites[indexPath.row].viewCount)"
            cell.dateLabel.text = favorites[indexPath.row].published_at
        } else {
            cell.bgImage.sd_setImage(with: URL(string: news[indexPath.row].image))
            cell.titleLabel.text = news[indexPath.row].name
            cell.viewsLabel.text = "\(news[indexPath.row].viewCount)"
            cell.dateLabel.text = news[indexPath.row].data
        }
        cell.favButton.isEnabled = false
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "DetailNews") as! DetailNewsViewController
        if isLogged {
            vc.detailNews = favorites[indexPath.row]
        } else {
            vc.realmDetail = news[indexPath.row]
        }
        self.show(vc, sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            if isLogged {
                let params = ["user_id": UserDefaults.standard.integer(forKey: "UserId"),
                              "article_id": favorites[indexPath.row].id]
                NetworkManager.makeRequest(target: .removeFavorite(params), success: { (json) in
                    self.loadData()
                })
            } else {
                realm.deleteObject(object: news[indexPath.row])
                getRealmObject()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height/5 + 20.0
    }
}

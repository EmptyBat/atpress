//
//  EditProfileController.swift
//  ernur.kz
//
//  Created by Toremurat on 13.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import CropViewController
import SkyFloatingLabelTextField
import SVProgressHUD


class EditProfileController: UITableViewController {

    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var nameField: SkyFloatingLabelTextField!
    @IBOutlet weak var avatarView: UIImageView!
    var avatarName: String = ""
    var email: String = ""
    
    static func instantiate() -> EditProfileController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileController") as! EditProfileController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Профиль өзгерту"
        avatarView.layer.cornerRadius = avatarView.frame.size.height/2
        avatarView.layer.borderColor = UIColor(red:0.97, green:0.93, blue:0.03, alpha:1.0).cgColor
        avatarView.layer.borderWidth = 3.0
        emailField.text = email
    }

  
    @IBAction func uploadButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
        let firstAction = UIAlertAction(title: "Выбрать фото", style: .default) { (_) in
            self.openPicker()
        }
        let secondAction = UIAlertAction(title: "Снять фото", style: .default) { (_) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func openPicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isCameraDeviceAvailable(.front) || UIImagePickerController.isCameraDeviceAvailable(.rear) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            SVProgressHUD.showError(withStatus: "Құрылғыңыз камераны қолдамайды")
        }
    }
    
    func saveProfile() {
        guard let name = nameField.text, !name.isEmpty, let email = emailField.text, !email.isEmpty else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
        }
        let id = UserDefaults.standard.integer(forKey: "UserId")
        var params = ["name": name, "email":email, "user_id": "\(id)"]
        if avatarName != "" {
            params["avatar"] = avatarName
        }
        print(params)
        NetworkManager.makeRequest(target: .updateProfile(params)) { (json) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateProfile"), object: nil, userInfo: ["status" : true])
            self.navigationController?.popViewController(animated: true)
            print(json)
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 {
            saveProfile()
        }
    }
}

extension EditProfileController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true) {
            self.cropPickedImage(image)
        }
    }
    
    func cropPickedImage(_ photo : UIImage) {
        
        let cropViewController = CropViewController(image: photo)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
}

extension EditProfileController : CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true)  {
            let data = UIImageJPEGRepresentation(image, 0.5)!
            NetworkManager.makeRequest(target: .uploadAvatar(data), success: {[unowned self] (json) in
                print(json)
                self.avatarView.image = image
                if let fileName = json["data"]["fileName"].string {
                    self.avatarName = fileName
                }
            })
            
        }
    }
}

//
//  ProfileController.swift
//  ernur.kz
//
//  Created by Toremurat on 13.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON


class ProfileController: UITableViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    private var name = ""
    private var email = ""
    let picker = UIImagePickerController()
    var pickedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        configureUI()
        loadData()
        setEmptyBackTitle()
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileController.subscribeObserver), name: NSNotification.Name(rawValue: "updateProfile"), object: nil)
        
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureUI() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.tableFooterView = UIView()
        
        avatarView.layer.cornerRadius = avatarView.frame.size.height/2
        avatarView.layer.borderColor = UIColor(red:0.97, green:0.93, blue:0.03, alpha:1.0).cgColor
        self.navigationController?.navigationBar.shadowImage = UIImage()
        avatarView.layer.borderWidth = 3.0
        
    }
    
    func loadData() {
        let id = UserDefaults.standard.integer(forKey: "UserId")
        NetworkManager.makeRequest(target: .getProfile(userId: id)) { [weak self] (json) in
            guard let vc = self else { return }
            let data = json["data"]
            vc.avatarView.sd_setImage(with: URL(string: data["avatar"].stringValue))
            vc.nameLabel.text = data["name"].stringValue
            vc.email = data["email"].stringValue
            print(json)
        }
    }
    
    func subscribeObserver(notification:NSNotification) -> Void {
        guard let userInfo = notification.userInfo, let status = userInfo["status"] as? Bool  else { return }
        if status {
            loadData()
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : 2
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0 where indexPath.row == 1:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommentsVC") as! CommentsViewController
            self.show(vc, sender: nil)
        case 1 where indexPath.row == 0:
            let vc = EditProfileController.instantiate()
            vc.email = email
            self.show(vc, sender: nil)
        case 1 where indexPath.row == 1:
            AuthManager.shared.logout()
            break
        default:
            break
        }
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel!.textColor = UIColor.gray
        header.textLabel!.font = UIFont.systemFont(ofSize: 15.0)
        
        header.textLabel!.frame = header.frame
        header.textLabel!.textAlignment = NSTextAlignment.left
    }
    
}
extension ProfileController {
    func refresh() {
        refreshBegin { (x:Int) in
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    func refreshBegin(refreshEnd:@escaping (Int) -> ()) {
        loadData()
        refreshEnd(0)
        
    }
}

//
//  ContactsController.swift
//  ernur.kz
//
//  Created by Toremurat on 10.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit

class ContactsController: UIViewController {
    
    var webView: UIWebView!
    var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        configureUI()
        menuButton = UIBarButtonItem(image: UIImage(named: "menu-copy"), style: .plain, target: self, action: nil)
        self.navigationController?.navigationItem.leftBarButtonItem = menuButton
        self.navigationController?.navigationBar.tintColor = UIColor.black
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if let url = URL(string: "hhttp://atpress.bugingroup.com/contact") {
            webView.loadRequest(URLRequest(url: url))
        }
       
    }
    
    func configureUI() {
        webView = UIWebView()
        webView.backgroundColor = UIColor.white
        view.addSubview(webView)
        webView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

}

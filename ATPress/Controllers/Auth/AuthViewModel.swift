//
//  AuthViewModel.swift
//  ernur.kz
//
//  Created by Toremurat on 23.05.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import Foundation
import SVProgressHUD
import UIKit

class AuthViewModel: NSObject {
    
    // MARK: - Auth API Routes
    
    func signIn(email: String, password: String) {
        NetworkManager.makeRequest(target: .signIn(params: ["email": email, "password": password])) { (json) in
            if let userId = json["data"]["id"].int {
                AuthManager.shared.login(userId)
            }
        }
    }
    
    func signUp(params: [String: Any]) {
        NetworkManager.makeRequest(target: .signUp(params: params), success: { (json) in
            if let userId = json["data"]["user_id"].int {
                AuthManager.shared.login(userId)
            }
        })
    }
    
    func forgotPassword(email: String, callBack: @escaping () -> Void) {
        NetworkManager.makeRequest(target: .forgotPassword(params: ["email" : email])) { (json) in
            SVProgressHUD.showSuccess(withStatus: "На ваш E-mail оптправлена ссылка с восстановлением пароля. Необходимо проверить почту.")
            callBack()
        }
    }
}

//
//  ViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.03.18.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    // MARK: - Variabels
    
    let viewModel = AuthViewModel()
    
    static func instantiate() -> LoginController {
        return LoginController.fromStoryboard(name: "Auth")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        hideKeyboardWhenTappedAround()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(sender: Notification) {
        self.scrollView.frame.origin.y = -50
    }
    
    func keyboardWillHide(sender: Notification) {
        self.scrollView.frame.origin.y = 0
    }
    
    // MARK: - ConfigureUI function
    
    func configureUI() {
    
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
       
    }
    
    // MARK: - Close UIButton Action
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Login UIButton Action
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        view.endEditing(true)
        guard let password = passwordTextField.text, !password.isEmpty, let email = emailTextField.text, !email.isEmpty  else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
        }
        viewModel.signIn(email: email, password: password)
    }

}

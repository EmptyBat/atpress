//
//  RegisterViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.03.18.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD

class RegisterController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    let viewModel = AuthViewModel()
    
    static func instantiate() -> RegisterController {
        return RegisterController.fromStoryboard(name: "Auth")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Регистрация"
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(sender: Notification) {
        self.view.frame.origin.y = -50
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(sender: Notification) {
        self.view.frame.origin.y = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Close UIButton Action
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Register UIButton Action
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        view.endEditing(true)
        guard let password = passwordTextField.text, !password.isEmpty, let email = emailTextField.text, !email.isEmpty, let confirm = confirmPasswordTextField.text, !confirm.isEmpty, let name = nameTextField.text, !name.isEmpty else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
        }
        if password.elementsEqual(confirm) {
            let params = ["name": name, "email": email, "password": password]
            viewModel.signUp(params: params)
        } else {
            SVProgressHUD.showError(withStatus: BaseMessage.passwordError)
        }
       
    }
    
    
}

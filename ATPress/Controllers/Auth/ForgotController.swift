//
//  ForgotPasswordViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.03.18.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class ForgotController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    let viewModel = AuthViewModel()
    
    static func instantiate() -> ForgotController {
        return ForgotController.fromStoryboard(name: "Auth")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    // MARK: - Send UIButton Action
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        guard let email = emailTextField.text, !email.isEmpty else {
             SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
        }
        viewModel.forgotPassword(email: email) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Close UIButton Action
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}

//
//  ViewController.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 24.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SwiftyJSON
import Presentr

class MainViewController: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    var categoryArr: [Rubric] = []
    var currentRubric: Int?
    
    //Comment: - You can do this shit thru - struct NewsCategory { var id: Int, var name: String }
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        setEmptyBackTitle()
        setupPageMenu()
        if let navCtrl = self.navigationController {
            setClearNavbar(Navigation: navCtrl)
        }
       
    }
    // MARK: - UI Setup
    func setupPageMenu(){
        // Navigation Title
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text = "ATPress.kz"
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.init(name: "Lato-Bold", size: 18.0)!
        navigationItem.titleView = titleLabel
        
        self.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        // Right Bar Button Items
        let currencyItem = UIBarButtonItem(image: UIImage(named: "icon_currency"), style: .plain, target: self, action: #selector(currencyButtonPressed))

   /*     let audioButton: UIButton = UIButton(type: .custom)
        audioButton.setImage(UIImage(named: "icon_audio"), for: [])
        audioButton.addTarget(self, action: #selector(audioButtonPressed), for: .touchUpInside)
        audioButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let audioItem = UIBarButtonItem(customView: audioButton)*/

        let reportItem = UIBarButtonItem(image: UIImage(named: "icon_reporter"), style: .plain, target: self, action: #selector(reportButtonPressed))
      //  self.navigationItem.rightBarButtonItems = [currencyItem, reportItem, audioItem]
         self.navigationItem.rightBarButtonItems = [currencyItem, reportItem]
        // MARK: - Scroll menu setup
        var controllerArray : [UIViewController] = []
   
        NetworkManager.makeRequest(target: .getCategories()) { [weak self] (json) in
            guard let vc = self else { return }
            var index = 0
            for (_, subJson):(String, JSON) in json["data"] {
                if let new = Rubric(json: subJson) {
                    if index == 0 {
                        let controller: MainNewsController = MainNewsController()
                        controller.title = "first".localized()
                        controller.categoryId = 0
                        controllerArray.append(controller)
                        
                        let controller_sub: NewsController = NewsController()
                        controller_sub.title = new.name
                        controller_sub.categoryId = new.id
                        controllerArray.append(controller_sub)
                    } else {
                        let controller: NewsController = NewsController()
                        controller.title = new.name
                        controller.categoryId = new.id
                        controllerArray.append(controller)
                    }
                    index = index + 1
                }
            }
            // Customize menu (Optional)
            let parameters: [CAPSPageMenuOption] = [
                .scrollMenuBackgroundColor(BaseMessage.appColor),
                .viewBackgroundColor(BaseMessage.appColor),
                .selectionIndicatorColor(UIColor.white),
                .selectionIndicatorHeight(1.0),
                .unselectedMenuItemLabelColor(UIColor(red:255, green:0, blue:0, alpha:0.5)),
                .selectedMenuItemLabelColor(UIColor.red),
                .menuItemFont(UIFont.init(name: "Lato-Regular", size: 16.0)!),
                .menuHeight(40.0),
                .menuItemWidthBasedOnTitleTextWidth(true),
                .addBottomMenuHairline(false),
                .centerMenuItems(false)
            ]
            
            // Initialize scroll menu
            vc.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: vc.view.frame.width, height: vc.view.frame.height), pageMenuOptions: parameters)
            
            vc.addChildViewController(vc.pageMenu!)
            vc.view.addSubview(vc.pageMenu!.view)
            vc.pageMenu!.didMove(toParentViewController: self)
            vc.pageMenu!.moveToPage((vc.currentRubric ?? 0))
        }
       
    }
    
    // MARK: UIBarButton Actions
    func currencyButtonPressed() {
        let presenter = Presentr(presentationType: .bottomHalf)
        let vc = CurrencyController()
        customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    func audioButtonPressed() {
     //   let vc = AudiosController.instantiate()
       // self.show(vc, sender: nil)
    }
    
    func reportButtonPressed() {
        let vc = ReportContactsController.instantiate()
        self.show(vc, sender: nil)
    }
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
}


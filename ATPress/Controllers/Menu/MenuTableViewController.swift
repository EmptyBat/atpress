//
//  MenuTableViewController.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 25.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Localize_Swift
class MenuTableViewController: UITableViewController {
    
    // Variables
    var data = ["news".localized(), "favorite".localized(), "Feedback".localized(), "marketing".localized(), "settings".localized()]

    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setText()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    @objc func setText(){
        data.removeAll()
        data = ["news".localized(), "favorite".localized(), "Feedback".localized(), "marketing".localized(), "settings".localized()]
     tableView.reloadData()
    }
    // MARK: Configuring UI
    func configUI() {
        tableView.register(MenuTableViewCell.self, forCellReuseIdentifier: "MenuCell")
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return data.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHeaderCell", for: indexPath) as! MenuHeaderCell
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuTableViewCell
            cell.selectionStyle = .none
            cell.setupData(data[indexPath.row])
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
       /* case 0 where indexPath.section == 0: // Auth or Profile
            if let _ = UserDefaults.standard.value(forKey: "UserId") {
                self.performSegue(withIdentifier: "SegueProfile", sender: nil)
            } else {
                let vc = LoginController.instantiate()
                self.present(vc, animated: true, completion: nil)
            }*/
           
        case 0 where indexPath.section == 1:
            self.performSegue(withIdentifier: "SeguePage", sender: nil)
        case 1 where indexPath.section == 1:
           self.performSegue(withIdentifier: "SegueFavorites", sender: nil)
        case 2 where indexPath.section == 1:
            self.performSegue(withIdentifier: "SegueWeb", sender: "Feedback".localized())
        case 3 where indexPath.section == 1:
            self.performSegue(withIdentifier: "SegueWeb", sender: "marketing".localized())
        case 4 where indexPath.section == 1:
            self.performSegue(withIdentifier: "SegueSettings", sender: nil)
        default:
            
            break
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 162.0
        default:
            return 60.0
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueWeb" {
            if let snd = sender as? String {
                let navVC = segue.destination as! UINavigationController
                let vc = navVC.topViewController as! WebViewController
                vc.titleName = snd
                if snd == "Feedback".localized() {
                    vc.urlString = "http://atpress.kz/728-kontakty"
                } else {
                    vc.urlString = "http://atpress.bugingroup.com/reklama"
                }
            }
        }
    }

}

//
//  DetailNewsViewController.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 25.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON
import SVProgressHUD
import SKPhotoBrowser
import ImageSlideshow
import AlamofireImage
class DetailNewsViewController: UIViewController, UIScrollViewDelegate, UIWebViewDelegate {
    
    @IBOutlet weak var slidebaner: ImageSlideshow!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var webHeight: NSLayoutConstraint!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var viewLabel: UILabel!
    var favBarButton: UIBarButtonItem!
    @IBOutlet weak var comment_btn_ht: NSLayoutConstraint!
    
    // MARK: Variables
    private var newsText = ""
    var sidebaner_height: NSLayoutConstraint?
    let realm = RealmManager()
    var detailNews:News?
    var baner:Baner?
    var loaded = false
    var alamofiresource:AlamofireSource?
    var configureComment:ConFigureComment?
    var realmDetail: RealmNews?
    var defaultSize  = ["textFontSize": 16]
    var styleCss = ""
    var news_id = 0
    var isFavorited: Bool = false
    var is_show: Bool = false
    var banerImages = [AlamofireSource]()
    var newsimages :[String] = []
    
    static func instantiate() -> DetailNewsViewController {
        return UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "DetailNews") as! DetailNewsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setEmptyBackTitle()
        setupPreferences()
        setupData()
        commentsStatus()
        SetBanerImages()
    }
    
    func configUI() {
        webView.delegate = self
        webView.scrollView.isScrollEnabled = false
        sidebaner_height = slidebaner.heightAnchor.constraint(equalToConstant: slidebaner.frame.height)
        sidebaner_height?.isActive = true
        let shareBarButton = UIBarButtonItem(image: UIImage(named: "icon_share"), style: .plain, target: self, action: #selector(shareButtonPressed))
        favBarButton = UIBarButtonItem(image: UIImage(named: "icon_fav_gr"), style: .plain, target: self, action: #selector(favoriteBarButtonPressed))
        
        let fontPlusButton = UIBarButtonItem(image: UIImage(named: "icon_fontPlus"), style: .plain, target: self, action: #selector(fontPlusAction))
        let fontMinusButton = UIBarButtonItem(image: UIImage(named: "icon_fontMinus"), style: .plain, target: self, action: #selector(fontMinusAction))
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: view.frame.height))
        titleLabel.text = "ПУБЛИКАЦИЯ"
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.init(name: "Lato-Bold", size: 16.0)!
        navigationItem.titleView = titleLabel
        self.navigationItem.rightBarButtonItems = [shareBarButton, favBarButton, fontPlusButton, fontMinusButton]
        
        // setup slidebaner
        _ = UIPageControl()
        slidebaner.slideshowInterval = 10.0
        slidebaner.contentScaleMode = UIViewContentMode.scaleAspectFill
        slidebaner.draggingEnabled = false
        slidebaner.pageIndicator = nil
        slidebaner.currentPageChanged = { page in
            print("current page:", page)
        }
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(DetailNewsViewController.banerTaped))
        slidebaner.addGestureRecognizer(recognizer)
    }
    // set baner images
       func SetBanerImages() {
        NetworkManager.makeRequest(target: .getBaner()) { [weak self] (json) in
            guard let vc = self else { return }
            print("asfsafsa",json)
            vc.baner = Baner.init(json: json["data"])
                vc.baner!.url = json["data"][0]["url"].stringValue
            let list = json["data"]
            for (index, element) in list.enumerated() {
                print(index, ":", element)
                self?.banerImages.append(AlamofireSource(urlString:"http://atpress.kz" + json["data"][index]["image"].stringValue)!)
                   self!.slidebaner.setImageInputs(self!.banerImages)
            }
            if (self?.banerImages.count == 0){
                self?.sidebaner_height?.constant = 0
            }
        }
    }
    // check: comments status
      func commentsStatus() {
        NetworkManager.makeRequest(target: .getConfigure()) { [weak self] (json) in
            guard let vc = self else { return }
            vc.configureComment = ConFigureComment.init(json: json["data"])
            if json["data"][0]["is_show"].boolValue {
                vc.configureComment!.is_show = json["data"][0]["is_show"].boolValue
            }
            if (vc.configureComment?.is_show == false){
                vc.commentsButton.isHidden = true
                vc.comment_btn_ht.constant = 0
                self?.view.layoutIfNeeded()
            }
        }
    }
    // MARK: Load data
    func loadData(id: Int) {
        SVProgressHUD.show()
        NetworkManager.makeRequest(target: .getNewsDetail(id: id)) { [weak self] (json) in
            guard let vc = self else { return }
            print ("exp == ",json)
            vc.detailNews = News.init(json: json["data"])
            if json["data"]["favorite"].boolValue {
                vc.favBarButton.image = UIImage(named: "icon_fav_red")
                vc.detailNews!.isFavorite = json["data"]["favorite"].boolValue
            }
            for (_, subJson):(String, JSON) in json["data"]["images"] {
                let image = Images(json: subJson)
                self?.newsimages.append(image.url)
                print ("tesssf",image.url)
        }
            if (self?.newsimages.count == 0){
                self?.newsimages.append(json["data"]["image"]["original"].stringValue)
                print("asfsfafas",json["data"]["image"]["original"].stringValue)
            }
            if let newsText = json["data"]["text"].string {
                print("<div>" + newsText + "</div>")
                vc.newsText = vc.styleCss + "<div>" + newsText + "</div>"
                let replaced = vc.newsText.replacingOccurrences(of: "<img src=\"", with: "<img src=\"http://atpress.kz")
                print("news saffas",replaced)
                vc.webView.loadHTMLString(replaced , baseURL: URL(string: BaseMessage.baseUrl))
            }
            
            if (vc.news_id != 0){
                vc.configUI()
                vc.setupPreferences()
                vc.commentsStatus()
                vc.SetBanerImages()
                if let detail = vc.detailNews {
                    vc.titleLabel.text = detail.name
                    vc.dateLabel.text = detail.published_at
                    vc.newsImage.sd_setImage(with: URL(string: detail.image))
                    
                } else if let detail = vc.realmDetail{
                    vc.title = detail.categoryName
                    vc.titleLabel.text = detail.name
                    vc.dateLabel.text = detail.data
                    vc.newsImage.sd_setImage(with: URL(string: detail.image))
                }
            }
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let surl = request.url?.absoluteString
        
        if loaded == true {
            return false;
        }
        return true;
    }
    func setupData() {
        if let detail = detailNews {
            titleLabel.text = detail.name
            dateLabel.text = detail.published_at
            newsImage.sd_setImage(with: URL(string: detail.image))
            if (news_id != 0){
            loadData(id: news_id)
            }
            else {
            loadData(id: detail.id)
            }
            
        } else if let detail = realmDetail{
            self.title = detail.categoryName
            titleLabel.text = detail.name
            dateLabel.text = detail.data
            newsImage.sd_setImage(with: URL(string: detail.image))
            if detail.text == "" {
                if (news_id != 0){
                    loadData(id: news_id)
                }
                else {
                    loadData(id: detail.id)
                }
            } else {
                
                webView.loadHTMLString(self.styleCss + detail.text, baseURL: URL(string: "http://infoboom.kz"))
            }
        }
        else {
             loadData(id: news_id)
        }
    }
    
    func setupPreferences() {
        let link = "<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>"
        
        styleCss = link + "<style> body,p, div {font-family: 'Lato', sans-serif; font-size: 16px; text-indent: 0 !important;} img {width: 100%;} iframe{width: 100%;}</style>"
        
        if let navCtrl = self.navigationController {
            setClearNavbar(Navigation: navCtrl)
        }
        
        // Check if a news already in favorite list
        if let objects = realm.getObjects(type: RealmNews.self) {
            for element in objects {
                if let obj = element as? RealmNews {
                    if obj.id == detailNews?.id ?? realmDetail?.id {
                        isFavorited = true
                        favBarButton.image = UIImage(named: "icon_fav_red")
                    }
                }
            }
        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loaded = true
        webHeight.constant =  webView.scrollView.contentSize.height
        SVProgressHUD.dismiss()
    }
    
    // MARK: - Comment button action
    @IBAction func commentButtonPressed(_ sender: UIButton) {
        if let detail = detailNews {
            let vc = CommentsViewController.instantiate()
            vc.articleId = detail.id
            self.show(vc, sender: nil)
        }
    }
    
    // MARK: - Change WebView Text Size
    func changeWebViewFontSize(zoomInOrZoomOut: Int, webView: UIWebView)
    {
        //1 = decreace, 2 = increace
        var textFontSizeTemp = defaultSize["textFontSize"]! as Int
        switch zoomInOrZoomOut {
            case 1:
                textFontSizeTemp  = textFontSizeTemp - 1
            case 2:
                textFontSizeTemp = textFontSizeTemp + 1
            default:
                break
        }
        defaultSize["textFontSize"] = textFontSizeTemp
        let fontCss = "<style>div, p{ font-size: \(textFontSizeTemp)px !important;}</style>"
        webView.loadHTMLString(fontCss + newsText , baseURL: URL(string: BaseMessage.baseUrl))
    }
    
    // MARK: - Font Minus Action
    func fontMinusAction() {
        changeWebViewFontSize(zoomInOrZoomOut: 1, webView: webView)
    }
    
    // MARK: - Font Plus Action
    func fontPlusAction() {
        changeWebViewFontSize(zoomInOrZoomOut: 2, webView: webView)
    }
    
    // MARK: - Share button action
    func shareButtonPressed() {
        if let obj = detailNews {
            let objectsToShare = [obj.name, obj.share_link]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - News Image touch action
    @IBAction func imageViewPressed(_ sender: UITapGestureRecognizer) {
        if detailNews != nil{
            var images = [SKPhoto]()
            for (_ , element) in newsimages.enumerated() {
                let photo = SKPhoto.photoWithImageURL(element.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!)
                print("my test for urls",element.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed));
                photo.shouldCachePhotoURLImage = true
                images.append(photo)
            }
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            present(browser, animated: true, completion: {})
        }
            else {
                var images = [SKPhoto]()
                for (_ , element) in newsimages.enumerated() {
                    let photo = SKPhoto.photoWithImageURL(element.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!)
                    print("my test for urls",element.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed));
                    photo.shouldCachePhotoURLImage = true
                    images.append(photo)
            }
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            present(browser, animated: true, completion: {})
            
        }
    }
    
    // MARK: - Share button action
    func favoriteBarButtonPressed() {
        if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int, let detail = detailNews {
            let params = ["user_id" : userId, "article_id": detail.id]
            if detail.isFavorite {
                NetworkManager.makeRequest(target: .removeFavorite(params), success: { (json) in
                     self.favBarButton.image = UIImage(named: "icon_fav_gr")
                })
            } else {
                NetworkManager.makeRequest(target: .addFavorite(params), success: { (json) in
                     self.favBarButton.image = UIImage(named: "icon_fav_red")
                })
            }
        } else {
            if let obj = detailNews {
                if isFavorited {
                    favBarButton.image = UIImage(named: "icon_fav_gr")
                    realm.removeArticleById(obj.id)
                } else {
                    favBarButton.image = UIImage(named: "icon_fav_red")
                    let obj = RealmNews(value: ["id": obj.id, "name": obj.name, "text": newsText, "categoryName": obj.categoryName, "image": obj.image, "commentCount": 0, "viewCount": obj.viewCount, "data": obj.published_at])
                    realm.saveObjects(objs: [obj])
                }
               
                
            }
        }
    }
    @objc func banerTaped() {
      //  let fullScreenController = slidebaner.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
     //   fullScreenController.slidebaner.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
   

}

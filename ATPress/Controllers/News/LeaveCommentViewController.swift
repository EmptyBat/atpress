//
//  LeaveCommentViewController.swift
//  egemen.kz
//
//  Created by Toremurat on 28.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import SkyFloatingLabelTextField

class LeaveCommentViewController: UIViewController, UITextViewDelegate {
    
    var articleId = 0
    
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var nameField: SkyFloatingLabelTextField!
    @IBOutlet weak var textView: UITextView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Пікір қосу"
        textView.delegate = self
        textView.textColor = UIColor.lightGray
        //textView.becomeFirstResponder()
        settext()
    }
    func settext() {
        nameField.placeholder = "name".localized()
        textView.text = "write_comment".localized()
    }
    override func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: UI Actions
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        guard let text = textView.text, !text.isEmpty, let email = emailField.text, !email.isEmpty, let name = nameField.text, !name.isEmpty else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
        }
       
        if !validateEmail(enteredEmail: email) {
            SVProgressHUD.showError(withStatus: "E-mail қате терілді")
            return
        }
        let params = ["name": name,
                      "email": email,
                      "text": text,
                      "article_id": "\(articleId)"]
        NetworkManager.makeRequest(target: .addComment(params: params)) { (json) in
            print(json)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateComments"), object: nil, userInfo: ["status" : true])
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    // MARK: TextView delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            sendButtonPressed(UIButton())
        }
        return true
    }
  
    
}

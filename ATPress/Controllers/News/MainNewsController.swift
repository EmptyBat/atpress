//
//  MainNewsController.swift
//  ernur.kz
//
//  Created by Toremurat on 05.04.18.
//  Copyright © 2018 Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage
import Localize_Swift
class MainNewsController: UITableViewController {
    
    //MARK: Variables
    let realmManager = RealmManager()
    var headerNews: [News] = []
    var lastNews: [News] = []
    
    var categoryId = 0
    var pageCount = 1
    var totalPage = 1
    
    var rehreshC: UIRefreshControl!
    var loadMoreStatus = false
    
    //MARK: IBOutlets
    @IBOutlet weak var menuBUtton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuBUtton.target = self.revealViewController()
            menuBUtton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        let notificationName = Notification.Name("push")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(newPush),
            name: notificationName,
            object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        configureUI()
        loadData()
        
    }
    @objc func reload(){
        loadData()
    }
    @objc func newPush(notification: NSNotification){
       let news_id = notification.object as! Int
        let vc = DetailNewsViewController.instantiate()
        vc.news_id = news_id
        self.show(vc, sender: nil)
    
    }
    
    // MARK: - Configure UI
    
    func configureUI() {
        //Table View
        tableView.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        tableView.registerNib(MainNewsCell.self)
        tableView.registerNib(NewsTableViewCell.self)
        tableView.registerNib(HeaderNewsTableViewCell.self)
        
        // RefrechControl
        rehreshC = UIRefreshControl()
        rehreshC.addTarget(self, action: #selector(refreshBegin), for: .valueChanged)
        rehreshC.attributedTitle = NSAttributedString(string: "update".localized())
        tableView.addSubview(rehreshC)
        
    }
    
    // MARK: - Refresh Control selectors
    func refreshBegin() {
        pageCount = 1
        DispatchQueue.global().async {
            sleep(1)
            self.loadData()
        }
    }
    
    func loadData() {
      //  SVProgressHUD.show()
        loadMoreStatus = false
        NetworkManager.makeRequest(target: .getLastArticles(page: pageCount, per_page: 3)) { [weak self] (json) in
            guard let vc = self else { return }
            if vc.rehreshC.isRefreshing {
                vc.headerNews.removeAll()
                vc.lastNews.removeAll()
                vc.rehreshC.endRefreshing()
            }
            print(json)
            for (_, subJson):(String, JSON) in json["data"]["data"] {
                let new = News(json: subJson)
                vc.headerNews.append(new)
            }
            vc.tableView.reloadData()
        }
        loadMoreStatus = false
        NetworkManager.makeRequest(target: .getLastArticles(page: pageCount, per_page: 5)) { [weak self] (json) in
            guard let vc = self else { return }
            let list = json["data"]["data"]
            for i in 1...list.count-3 {
                let new = News(json: json["data"]["data"][i+2])
                vc.lastNews.append(new)
            }
            
            vc.totalPage = json["data"]["total_page"].intValue
            print ("tesedt",json["data"]["total_page"].intValue)
            //  SVProgressHUD.dismiss()
            vc.tableView.reloadData()
        }
    }
    
    // MARK: Fetching data from api
    func loadLastArticles() {
        loadMoreStatus = false
        NetworkManager.makeRequest(target: .getLastArticles(page: pageCount, per_page: 5)) { [weak self] (json) in
            guard let vc = self else { return }
            let list = json["data"]["data"]
            for i in 0...list.count-3 {
                let new = News(json: json["data"]["data"][i+2])
                vc.lastNews.append(new)
            }
            
            vc.totalPage = json["data"]["total_page"].intValue
            print ("tesedt",json["data"]["total_page"].intValue)
          //  SVProgressHUD.dismiss()
            vc.tableView.reloadData()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            
            if loadMoreStatus {
                pageCount += 1
                loadLastArticles()
            }
        }
    }
    
}
// MARK: - Table view data sourcse

extension MainNewsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return headerNews.count
        default:
            return lastNews.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let cell: HeaderNewsTableViewCell = tableView.dequeReusableCell(for: indexPath)
                cell.setupData(headerNews[indexPath.row])
                return cell
            } else {
                let cell: MainNewsCell = tableView.dequeReusableCell(for: indexPath)
                cell.setupData(headerNews[indexPath.row])
                
                cell.addCompletion = {
                    self.realmManager.saveArticle(news: self.headerNews[indexPath.row])
                }
                cell.removeCompletion = {
                    self.realmManager.removeArticleById(self.headerNews[indexPath.row].id)
                }
                return cell
            }
        default:
            
            
            
            let cell: NewsTableViewCell = tableView.dequeReusableCell(for: indexPath)
            cell.setupData(lastNews[indexPath.row])
            cell.addCompletion = {
                self.realmManager.saveArticle(news: self.lastNews[indexPath.row])
            }
            cell.removeCompletion = {
                self.realmManager.removeArticleById(self.lastNews[indexPath.row].id)
            }
            return cell
            
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let vc = DetailNewsViewController.instantiate()
        switch indexPath.section {
        case 0:
            vc.detailNews = headerNews[indexPath.row]
        default:
            vc.detailNews = lastNews[indexPath.row]
        }
        self.show(vc, sender: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = TableHeaderView()
        headerView.headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 : 60.0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 1 ? "info".localized() : ""
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let items = tableView.numberOfRows(inSection: 1)
        if indexPath.row == items - 1 && indexPath.row != 0 && indexPath.section == 1 && pageCount != totalPage {
            loadMoreStatus = true
            print("load more")
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return indexPath.row == 0 ? 200.0 : self.view.frame.height/5-20
        default:
     
                return self.view.frame.height/5
        }
    }
}



//
//  MainContentTableViewController.swift
//  egemen.kz
//
//  Created by Toremurat Zholayev on 25.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage
import Localize_Swift
class NewsController: UITableViewController {
    
    //MARK: - Indentifiers
    
    let headerIdentifier = "HeaderNewsCell"
    let newsIdentifier = "NewsCell"
    
    //MARK: - Variables
    
    let realm = RealmManager()
    var news: [News] = []
    var categoryId = 0
    var pageCount = 1
    var totalPage = 1
    var rehreshC: UIRefreshControl!
    var loadMoreStatus = false
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var menuBUtton: UIBarButtonItem!
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuBUtton.target = self.revealViewController()
            menuBUtton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        configureUI()
        loadData()
    }
    
    // MARK: - Congiure UI
    
    func configureUI() {
        //Table View
        tableView.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        tableView.registerNib(NewsTableViewCell.self)
        tableView.registerNib(HeaderNewsTableViewCell.self)
        
        // RefrechControl
        rehreshC = UIRefreshControl()
        rehreshC.addTarget(self, action: #selector(refreshBegin), for: .valueChanged)
        rehreshC.attributedTitle = NSAttributedString(string: "update".localized())
        tableView.addSubview(rehreshC)
    }
    
    // MARK: - Refresh Control selectors
    
    func refreshBegin() {
        pageCount = 1
        DispatchQueue.global().async {
            sleep(1)
            self.loadData()
        }
    }
    
    // MARK: Realm Saving
    
    func saveArticle(news: News) {
        let obj = RealmNews(value: ["id": news.id, "name": news.name, "text": "", "categoryName": news.categoryName, "image": news.image, "commentCount": 0, "viewCount": news.viewCount, "data": news.published_at])
        realm.saveObjects(objs: [obj])
    }
    
    // MARK: Fetching data from api
    func loadData() {
        SVProgressHUD.show()
        loadMoreStatus = false
        NetworkManager.makeRequest(target: .getNewsByCats(id: categoryId, page: pageCount)) { [weak self] (json) in
            guard let vc = self else { return }
            if vc.rehreshC.isRefreshing {
                vc.news.removeAll()
                vc.rehreshC.endRefreshing()
            }
            for (_, subJson):(String, JSON) in json["data"]["data"] {
                let new = News(json: subJson)
                vc.news.append(new)
            }
            vc.totalPage = json["data"]["total_page"].intValue
            if json["data"].count == 0 {
                vc.pageCount = 1
            }
              SVProgressHUD.dismiss()
            vc.tableView.reloadData()
        }
    }
    
}

// MARK: - TableView Deleghate & DataSource

extension NewsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
            let cell: NewsTableViewCell = tableView.dequeReusableCell(for: indexPath)
            cell.setupData(news[indexPath.row])
            
            cell.addCompletion = { _ in
                self.realm.saveArticle(news: self.news[indexPath.row])
            }
            cell.removeCompletion = { _ in
                self.realm.removeArticleById(self.news[indexPath.row].id)
            }
            
            return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let vc = DetailNewsViewController.instantiate()
        vc.detailNews = news[indexPath.row]
        self.show(vc, sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let items = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == items - 1 && indexPath.row != 0 && pageCount != totalPage{
            loadMoreStatus = true
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return self.view.frame.height/5
    }
    
}

// MARK: - Scroll View Did Scroll Delegate

extension NewsController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            if loadMoreStatus {
                pageCount += 1
                loadData()
            }
        }
    }
    
}

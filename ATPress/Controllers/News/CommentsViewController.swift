//
//  CommentsViewController.swift
//  egemen.kz
//
//  Created by Toremurat on 28.12.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class CommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var leaveButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // Variables
    var comments: [Comment] = []
    var articleId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(subscribeObserver), name: NSNotification.Name(rawValue: "updateComments"), object: nil)
        
        setEmptyBackTitle()
        configUI()
        
        loadData()
        
    }
    
    static func instantiate() -> CommentsViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommentsVC") as! CommentsViewController
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func configUI() {
        self.title = "Пікірлер"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 70
    }
    
    // MARL: Getting data from API
    func loadData() {
        // User comments
        if articleId == 0 {
            leaveButton.isHidden = true
            let id = UserDefaults.standard.integer(forKey: "UserId")
            NetworkManager.makeRequest(target: .getUserComments(userId: id), success: { [weak self] (json) in
                guard let vc = self else { return }
                for (_,subJson):(String, JSON) in json["data"] {
                    vc.comments.append(Comment.init(json: subJson))
                }
                vc.tableView.reloadData()
            })
        } else {
            NetworkManager.makeRequest(target: .getNewsComment(id: articleId)) {[weak self]  (json) in
                guard let vc = self else { return }
                vc.comments.removeAll()
                for (_,subJson):(String, JSON) in json["data"] {
                    vc.comments.append(Comment.init(json: subJson))
                }
                vc.tableView.reloadData()
            }
        }
       
    }
    
    // MARK: Observer handler
    func subscribeObserver(notification:NSNotification) -> Void {
        guard let userInfo = notification.userInfo, let status = userInfo["status"] as? Bool  else { return }
        if status {
            loadData()
        }
    }
    
    // MARK: - Send comment button action
    @IBAction func leaveCommentButtonPressed(_ sender: UIButton) {
     //   if let _ = UserDefaults.standard.value(forKey: "UserId") {
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeaveCommentVC") as! LeaveCommentViewController
            vc.articleId = articleId
            self.show(vc, sender: nil)
       /* } else {
            SVProgressHUD.showInfo(withStatus: "Пікір жазу үшін жүеге тіркелу қажет")
        }*/
    }
    
    // MARK: - TableView delegate and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableViewCell
        cell.setupData(comments[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

}

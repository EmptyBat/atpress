//
//  APIManager.swift
//  Egemen.kz
//
//  Created by Toremurat on 03.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import UIKit
import Foundation
import Moya
import Alamofire

let tmProvider = MoyaProvider<APIEndpoint>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

public enum APIEndpoint {
    
    // Auth
    case signUp(params: [String: Any])
    case signIn(params: [String: Any])
    case forgotPassword(params: [String: Any])
    
    // User
    case saveUserSettings([String: Any])
    case uploadAvatar(Data)
    case getProfile(userId: Int)
    case getUserComments(userId: Int)
    case updateProfile([String: Any])
    
    // News
    case getCategories()
    case getMainNews()
    case getNewsByCats(id: Int, page: Int)
    case getNewsDetail(id: Int)
    case getLastArticles(page: Int, per_page:Int)
    case getNewsComment(id: Int)
    case addComment(params: [String: Any])
    case getBaner()
    case getConfigure()
    
    // Favorite
    case getFavorites(userId: Int)
    case addFavorite([String: Any])
    case removeFavorite([String: Any])
    
    // Other
    case getCurrency()
    case getAudios()
    
    // Report
    case uploadReportImage(Data,user_name : String,email : String ,phone : String )
    case addReport([String: Any])
    
    // Pages
    case contactsPage()
    case advertsPage()
    //push
    case addtoken([String: Any])
    
}

extension APIEndpoint: TargetType {
    
    public var baseURL: URL { return URL(string: "http://atpress.kz/api")! }
    
    public var path: String {
        switch self {
            
        // Auth
        case .signUp:
            return "/user/sign"
        case .signIn:
            return "/user/login"
        case .forgotPassword:
            return "/user/password-reset"
            
        // User
        case .saveUserSettings:
            return "/user/save-settings"
        case .uploadAvatar:
            return "/user/upload-avatar"
        case .getProfile:
            return "/user/profile"
        case .getUserComments:
            return "/user/comments"
        case .updateProfile:
            return "/user/save-profile"
            
        // Favorite
        case .getFavorites:
            return "/user/featured-articles"
        case .addFavorite:
            return "/user/add-featured-article"
        case .removeFavorite:
            return "/user/remove-featured-article"
            
        // News
        case .getCategories:
            return "/category/main"
        case .getLastArticles:
            return "/article/last"
        case .getMainNews:
            return "/article/featured"
        case .getNewsByCats(let id, _):
            return "/article/getByCategory/\(id)"
        case .getNewsDetail(let id):
            return "/article/get/\(id)"
        case .getNewsComment(let id):
            return "/article/commentsByArticle/\(id)"
        case .addComment:
            return "/article/add-comment"
        case .getBaner:
            return "/banner"
        case .getConfigure:
            return "/configure"
            
        // Report
        case .addReport:
            return "/report/add"
        case .uploadReportImage:
            return "/article/upload"
            
        // Other
        case .getCurrency:
            return "/main/currency"
        case .getAudios:
            return "/audio/last"
            
        // Pages
        case .contactsPage:
            return "/article/view/contact_m"
        case .advertsPage:
            return "/article/view/ads_m"
        // push
        case .addtoken:
            return "/push"
            
        default:
            return ""
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .signIn, .signUp, .forgotPassword, .addComment, .uploadReportImage,
             .addReport, .addFavorite, .getFavorites, .removeFavorite, .uploadAvatar,
             .getProfile, .updateProfile, .getUserComments,.addtoken:
            return .post
        default:
            return .get
        }
    }
    
    public var parameters: [String: Any]? {
        
        var params:[String: Any] = [:] // for post params
        var query:[String: Any] = [:] // for get params
        
        switch self {
            
        case .signUp(let dict), .signIn(let dict), .forgotPassword(let dict):
            params["body"] = dict
            break
        case .getNewsDetail(_):
            if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int {
                query["user_id"] = userId
            }
        case .getMainNews():
            if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int {
                query["user_id"] = userId
            }
        case .getNewsByCats(_, let page):
            query["page"] = page
            query["per_page"] = 10
            if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int {
                query["user_id"] = userId
            }
            print(query)
            break
        case .getLastArticles(let page, let per_page):
            query["page"] = page
            query["per_page"] = per_page
            if let userId = UserDefaults.standard.value(forKey: "UserId") as? Int {
                query["user_id"] = userId
            }
            print(query)
            break
        // Report
        case .addReport(let dict):
            params["body"] = dict
            break
        case .saveUserSettings(let dict):
            params["body"] = dict
            break
        case .getFavorites(let userId):
            params["body"] = ["user_id": userId]
            break
        case .addFavorite(let dict):
            params["body"] = dict
            break
        case .removeFavorite(let dict):
            params["body"] = dict
            break
            
        // Profile
        case .getProfile(let userId):
            params["body"] = ["user_id": userId]
        case .updateProfile(let dict):
            params["body"] = dict
        case .getUserComments(let userId):
            params["body"] = ["user_id": userId]
            
        case .addComment(let dict):
            params["body"] = dict
            break
        //push
        case .addtoken(let dict):
            params["body"] = dict
            break
        default:
            break
        }
        
        // for query - params["query"], POST - params["body"]
        params["query"] = query
        return params
    }
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        case .signIn, .signUp, .forgotPassword, .addComment, .uploadReportImage,
             .addReport, .addFavorite, .getFavorites, .removeFavorite, .uploadAvatar,
             .getProfile, .updateProfile, .getUserComments:
            return CompositeJsonEncoding()
        default:
            return CompositeEncoding()
        }
        
    }
    
    public var sampleData: Data {
        return "Default sample data".data(using: String.Encoding.utf8)!
    }
    
    public var task: Task {
        switch self {
        case .uploadAvatar(let data):
            let imgData = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(data), name: "file", fileName: "userAva.jpg", mimeType: "image/jpeg")
            let multipartData = [imgData]
            return .upload(.multipart(multipartData))
        case .uploadReportImage(let data, let user_name,let email , let phone):
            let imgData = MultipartFormData(provider: .data(data), name: "file", fileName: "userAva.jpg", mimeType: "image/jpeg")
            //let multipartData = [imgData]
             let user_nameData = MultipartFormData(provider: .data(user_name.data(using: .utf8)!), name: "user_name")
             let emailData = MultipartFormData(provider: .data(email.data(using: .utf8)!), name: "email")
             let phoneData = MultipartFormData(provider: .data(phone.data(using: .utf8)!), name: "phone")
            let multipartData = [imgData, user_nameData, emailData,phoneData]
            return .upload(.multipart(multipartData))
        default:
            return .request
        }
    }
    
    public var validate: Bool {
        return true
    }
    
    public var headers: [String : String]? {
        let params:[String: String] = ["lang" : "lang".localized()]
        return params
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

struct CompositeEncoding: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        guard let parameters = parameters else {
            return try urlRequest.asURLRequest()
        }
        let queryParameters = (parameters["query"] as! Parameters)
        var queryRequest = try URLEncoding(destination: .queryString).encode(urlRequest, with: queryParameters)
        if let body = parameters["body"] {
            let bodyParameters = (body as! Parameters)
            
            let req = try URLEncoding(destination: .queryString).encode(urlRequest, with: bodyParameters)
            if let url = req.url, let query = url.query {
                queryRequest.httpBody = query.data(using: .utf8)
            }
            
            let params:[String: String] = ["lang" : "lang".localized()]
            queryRequest.allHTTPHeaderFields = params
            return queryRequest
        } else {
            let params:[String: String] = ["lang" : "lang".localized()]
            queryRequest.allHTTPHeaderFields = params
            return queryRequest
        }

        
    }
}

struct CompositeJsonEncoding: ParameterEncoding {
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        guard let parameters = parameters else {
            return try urlRequest.asURLRequest()
        }
        
        let queryParameters = (parameters["query"] as! Parameters)
        var queryRequest = try URLEncoding(destination: .queryString).encode(urlRequest, with: queryParameters)
        if let body = parameters["body"] {
            let bodyParameters = (body as! Parameters)
            do {
                let data = try JSONSerialization.data(withJSONObject: bodyParameters, options: [])
                
                if queryRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                    queryRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                queryRequest.httpBody = data
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        let params:[String: String] = ["lang" : "lang".localized()]
        queryRequest.allHTTPHeaderFields = params
        return queryRequest
    }
}
private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}


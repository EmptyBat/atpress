//
//  MoyaAdapter.swift
//  juzEntertainment
//
//  Created by Toremurat on 03.12.17.
//  Copyright © 2017 Toremurat. All rights reserved.
//

import Foundation
import UIKit
import Moya
import Alamofire
import SVProgressHUD
import SwiftyJSON

struct NetworkManager {

    static let provider = MoyaProvider<APIEndpoint>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    static func makeRequest(target: APIEndpoint, success successCallback: @escaping (JSON) -> Void) {
        
    
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.show()
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    let json = try! JSON(data: response.data)
                    print(json)
                    SVProgressHUD.dismiss()
                    if json["status"].string == "success"  || json["status"].boolValue == true {
                        successCallback(json)
                    } else {
                        SVProgressHUD.showError(withStatus: json["error"].string ?? json["data"]["error"].stringValue)
                    }
                } else {
                    SVProgressHUD.showError(withStatus: "\(BaseMessage.parsingError) - \(response.statusCode)")
                }
            case .failure(let error):
                print (error)
              //  SVProgressHUD.showError(withStatus: error.errorDescription)
            }
        }
    }
}

